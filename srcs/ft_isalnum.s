global _ft_isalnum

extern _ft_isdigit
extern _ft_isalpha

; int		ft_isalnum(int c);

_ft_isalnum:
	push rbp
	mov rbp, rsp
	xor rax, rax
	call _ft_isdigit
	cmp rax, 0
	jne .end
	call _ft_isalpha
	.end:
		leave
		ret
