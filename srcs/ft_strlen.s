global _ft_strlen

; size_t	ft_strlen(char const *str);

_ft_strlen:
	xor rax, rax
	xor rcx, rcx
	mov rdx, rdi
	cmp rdi, 0
	je .end
	not rcx
	repne scasb
	not rcx
	dec rcx
	mov rax, rcx
	mov rdi, rdx
	.end:
		ret