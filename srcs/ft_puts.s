global _ft_puts

extern _ft_putstr

; int		ft_puts(const char *s);

_ft_puts:
	push rbp
	mov rbp, rsp
	cmp rdi, 0
	je .nullValue
	.write:
		call _ft_putstr
		xor rax, rax
		mov rdi, 1
		lea rsi, [rel .eol]
		mov rdx, 1
		mov rax, 0x2000004
		syscall
		mov rax, 10
		leave
		ret

.nullValue:
	lea rdi, [rel .null]
	jmp .write

.null:
	db "(null)", 0

.eol:
	db 10
