global _ft_isdigit

; int		ft_isdigit(int c);

_ft_isdigit:
	xor rax, rax
	cmp rdi, 48
	jl .end
	cmp rdi, 57
	jg .end
	mov rax, 1
	.end:
		ret
