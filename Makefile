# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: revers <revers@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/11/23 16:38:49 by triviere          #+#    #+#              #
#    Updated: 2016/03/01 00:11:26 by revers           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a
TEST = test
ifeq ($(USER),revers)
	CMD_CP = /usr/local/Cellar/nasm/2.11.08_1/bin/nasm
else
	CMD_CP= $$HOME/.brew/Cellar/nasm/2.11.08_1/bin/nasm
endif
INCLUDES=-Iincludes
LIBS = -L . -lft

FLAG = -f macho64

DIR_SRC = srcs
DIR_OBJ = objs
DIR_INCLUDES = includes/
DIR_OTHER = ./
DEPS = includes/libft.h

SRC = $(shell find $(DIR_SRC) -type f)
OBJ = $(patsubst $(DIR_SRC)/%,$(DIR_OBJ)/%, $(SRC:.s=.o))

all: $(NAME)

test:
	gcc -Wall -Werror -Wextra main.c -o $(TEST) libft.a

$(DIR_OBJ)/%.o: $(DIR_SRC)/%.s
	@mkdir -p $(dir $@)
	$(CMD_CP) $(FLAG) $< -o $@

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

clean:
	rm -rf $(DIR_OBJ)

fclean: clean
	rm -f $(NAME) $(TEST)

re: fclean all

.PHONY : all clean fclean re test
