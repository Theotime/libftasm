global _ft_strchr

extern _ft_putchar
; char		*ft_strchr(char const *s, int c);

_ft_strchr:
	xor rcx, rcx
	mov rcx, rsi

	.while:
		mov rax, rdi
		cmp byte[rdi], cl
		je .end
		cmp byte [rdi], 0
		je .end
		inc rdi
		jmp .while

	.end:
		ret