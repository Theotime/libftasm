global _ft_strcat

; char		*ft_strcat(char *s1, const char *s2);
extern _ft_strlen
extern _ft_memcpy

_ft_strcat:
	push rbp
	mov rbp, rsp
	push rdi
	call _ft_strlen
	lea rcx, [rdi + rax]
	push rcx
	mov rdi, rsi
	call _ft_strlen
	mov rdx, rax
	mov rsi, rdi
	pop rdi
	call _ft_memcpy
	.end:
		pop rax
		leave
		ret
