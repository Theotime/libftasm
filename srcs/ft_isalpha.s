global _ft_isalpha

; int		ft_isalpha(int c);

_ft_isalpha:
	xor rax, rax
	cmp rdi, 65
	jl .end
	cmp rdi, 90
	jg .forLower
	mov rax, 1
	jmp .end

	.forLower:
		cmp rdi, 97
		jl .end
		cmp rdi, 122
		jg .end
		mov rax, 1

	.end:
		ret
