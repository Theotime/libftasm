global _ft_strdup

extern _ft_strlen
extern _ft_memcpy
extern _ft_bzero
extern _ft_strnew

; char		*ft_strdup(const char *s); 

_ft_strdup:
	push rbp
	mov rbp, rsp

	push rdi
	call _ft_strlen
	push rax
	mov rdi, rax
	call _ft_strnew

	pop rdx
	pop rsi
	mov rdi, rax
	call _ft_memcpy
	leave
	ret