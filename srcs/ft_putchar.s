global _ft_putchar

; void		ft_putchar(const char c);

extern _write

_ft_putchar:
	push rbp
	mov rbp, rsp
	sub rsp, 8
	mov rax, rdi
	mov byte [rbp - 8], al
	mov rsi, rbp
	sub rsi, 8
	mov rdx, 1
	mov rdi, 1
	call _write
	leave
	ret