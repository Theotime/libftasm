global _ft_bzero

; void»»···ft_bzero(void *s, size_t n);

extern _ft_memset


_ft_bzero:
	push rbp
	mov rbp, rsp
	mov rdx, rsi
	mov rsi, 0
	call _ft_memset	
	leave
	ret