#include "includes/libft.h"
#include <unistd.h>


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <fcntl.h>

#define T_MIN -512
#define T_MAX 512

int			main(int ac, char **av)
{
	char		*str;
	char		*str2;
	int			len;
	char		error;
	int 		fd;

	setbuf(stdout, NULL);

	if (ac == 2) {
		fd = open(av[1], O_RDONLY);
	} else {
		fd = 0;
	}
	ft_cat(fd);

	printf("##############################################################\n");
	printf("#                          Partie I                          #\n"); 
	printf("#                     Les fonction simples                   #\n");
	printf("##############################################################\n");

	/**
	* Test de ft_bzero
	**/
	printf("#     - ft_bzero:                                            #\n");
	str = strdup("Pince mi et pince moi sont dans un bateau,");
	len = ft_strlen(str);
	printf("#        - %-49s #\n", str);
	ft_bzero(str, len);
	printf("#        -                                                   #");
	write(1, str, len);
	write(1, "\n", 1);
	free(str);
	str = strdup("Pince mi tombe a l'eau");
	len = ft_strlen(str);
	printf("#        - %s                            #\n", str);
	ft_bzero(str, len);
	printf("#        -                                                   #");
	write(1, str, len);
	write(1, "\n", 1);
	free(str);
	str = strdup("Qui est ce qui reste ?");
	len = ft_strlen(str);
	printf("#        - %-49s #\n", str);
	ft_bzero(str, len);
	printf("#        -                                                   #");
	write(1, str, len);
	write(1, "\n", 1);
	free(str);

	/**
	* Test de ft_strcat
	**/
	printf("#     - ft_strcat:                                           #\n");
	str = strdup("Toto et tata");
	str = realloc(str, 50);
	str2 = strdup(" sont dans un bateau !");
	printf("#        - \"%s\" + \"%s\"         #\n", str, str2);
	printf("#            -> %-44s #\n", ft_strcat(str, str2));


	str = strdup("Toto");
	str = realloc(str, 50);
	str2 = strdup(" tombe a l'eau");
	printf("#        - \"%s\" + \"%s\"                         #\n", str, str2);
	printf("#            -> %-44s #\n", ft_strcat(str, str2));

	str = strdup("Qui est-");
	str = realloc(str, 50);
	str2 = strdup("ce qu'il reste ?");
	printf("#        - \"%s\" + \"%s\"                   #\n", str, str2);
	printf("#            -> %-44s #\n", ft_strcat(str, str2));

	/**
	* Test de ft_isalpha
	**/
	printf("#     - ft_isalpha:                                          #\n");
	error = 0;
	for (int i = T_MIN; i < T_MAX; ++i) {
		if (isalpha(i) != ft_isalpha(i)) {
			if (!error)
				error = 1;
			printf("    %d       -> %d | %d [ERROR]\n", i, isalpha(i), ft_isalpha(i));
		}
	}
	if (!error)
		printf("#        - de %-5d à %-5d                      [ SUCCESS ] #\n", T_MIN, T_MAX);
	
	/**
	* Test de ft_isdigit
	**/
	printf("#     - ft_isdigit:                                          #\n");
	error = 0;
	for (int i = T_MIN; i < T_MAX; ++i) {
		if (isdigit(i) != ft_isdigit(i)) {
			if (!error)
				error = 1;
			printf("    %d       -> %d | %d [ERROR]\n", i, isdigit(i), ft_isdigit(i));
		}
	}
	if (!error)
		printf("#        - de %-5d à %-5d                      [ SUCCESS ] #\n", T_MIN, T_MAX);

	/**
	* Test de ft_isalnum
	**/
	printf("#     - ft_isalnum:                                          #\n");
	error = 0;
	for (int i = T_MIN; i < T_MAX; ++i) {
		if (isalnum(i) != ft_isalnum(i)) {
			if (!error)
				error = 1;
			printf("	%d       -> %d | %d [ERROR]\n", i, isalnum(i), ft_isalnum(i));
		}
	}
	if (!error)
		printf("#        - de %-5d à %-5d                      [ SUCCESS ] #\n", T_MIN, T_MAX);

	/**
	* Test de ft_isascii
	**/
	printf("#     - ft_isascii:                                          #\n");
	error = 0;
	for (int i = T_MIN; i < T_MAX; ++i) {
		if (isascii(i) != ft_isascii(i)) {
			if (!error)
				error = 1;
			printf("    %d       -> %d | %d [ERROR]\n", i, isascii(i), ft_isascii(i));
		}
	}
	if (!error)
		printf("#        - de %-5d à %-5d                      [ SUCCESS ] #\n", T_MIN, T_MAX);

	/**
	* Test de ft_isprint
	**/
	printf("#     - ft_isprint:                                          #\n");
	error = 0;
	for (int i = T_MIN; i < T_MAX; ++i) {
		if (isprint(i) != ft_isprint(i)) {
			if (!error)
				error = 1;
			printf("    %d       -> %d | %d [ERROR]\n", i, isprint(i), ft_isprint(i));
		}
	}
	if (!error)
		printf("#        - de %-5d à %-5d                      [ SUCCESS ] #\n", T_MIN, T_MAX);

	/**
	* Test de ft_toupper
	**/
	printf("#     - ft_toupper:                                          #\n");
	error = 0;
	for (int i = T_MIN; i < T_MAX; ++i) {
		if (toupper(i) != ft_toupper(i)) {
			if (!error)
				error = 1;
			printf("	%d       -> %d | %d [ERROR]\n", i, toupper(i), ft_toupper(i));
		}
	}
	if (!error)
		printf("#        - de %-5d à %-5d                      [ SUCCESS ] #\n", T_MIN, T_MAX);

	/**
	* Test de ft_tolower
	**/
	printf("#     - ft_tolower:                                          #\n");
	error = 0;
	for (int i = T_MIN; i < T_MAX; ++i) {
		if (tolower(i) != ft_tolower(i)) {
			if (!error)
				error = 1;
			printf("	%d       -> %d | %d [ERROR]\n", i, tolower(i), ft_tolower(i));
		}
	}
	if (!error)
		printf("#        - de %-5d à %-5d                      [ SUCCESS ] #\n", T_MIN, T_MAX);

	/**
	* Test de ft_puts
	**/
	printf("#     - ft_puts:                                             #\n");
	ft_puts("#        - Pince mi et pince moi sont dans un bateau,        #");
	ft_puts("#        - Pince mi tombe a l'eau..                          #");
	ft_puts("#        - Qui est ce qui reste ?                            #");
	printf("##############################################################\n");

	printf("\n");
	printf("\n");
	printf("\n");


	printf("##############################################################\n");
	printf("#                         Partie II                          #\n"); 
	printf("#      Les fonction simples mais un peu moins de la libc     #\n");
	printf("##############################################################\n");


	/**
	* Test de ft_strlen
	**/
	printf("#    - ft_strlen:                                            #\n");
	str = strdup("Aujourd'hui il fait beau");
	printf("#        - %4zu | %4zu : %-35s #\n", strlen(str), ft_strlen(str), str);
	free(str);
	str = strdup("Quel bonheur cette correction");
	printf("#        - %4zu | %4zu : %-35s #\n", strlen(str), ft_strlen(str), str);
	free(str);
	str = strdup("Oui il y a surement des fautes");
	printf("#        - %4zu | %4zu : %-35s #\n", strlen(str), ft_strlen(str), str);
	free(str);
	str = strdup("Mais au finale je reste drole");
	printf("#        - %4zu | %4zu : %-35s #\n", strlen(str), ft_strlen(str), str);
	free(str);
	str = strdup("Je ne sais plus quoi mettre");
	printf("#        - %4zu | %4zu : %-35s #\n", strlen(str), ft_strlen(str), str);
	free(str);
	str = strdup("Mais on s'en fou c'est un test");
	printf("#        - %4zu | %4zu : %-35s #\n", strlen(str), ft_strlen(str), str);
	free(str);
	str = strdup("Pourvus que ca plante pas.... ");
	printf("#        - %4zu | %4zu : %-35s #\n", strlen(str), ft_strlen(str), str);
	free(str);


	/**
	* Test de ft_memset
	**/
	printf("#    - ft_memset:                                            #\n");
	str = malloc(200);
	ft_bzero(str, 200);
	ft_memset(str, '*', 24);
	printf("#        - 24 : * -> %-39s #\n", str);
	ft_bzero(str, 200);
	ft_memset(str, '#', 5);
	printf("#        - 5  : # -> %-39s #\n", str);
	ft_bzero(str, 200);
	ft_memset(str, 'a', 8);
	printf("#        - 8  : a -> %-39s #\n", str);
	ft_bzero(str, 200);
	ft_memset(str, 'X', 30);
	printf("#        - 30 : X -> %-39s #\n", str);
	free(str);

	/**
	* Test de ft_memcpy
	**/
	printf("#    - ft_memcpy:                                            #\n");
	str = strdup("Hello les gens !");
	str2 = strdup("Hallo");
	printf("#        - %s | %s                          #\n", str, str2);
	printf("#            -> %-44s #\n", ft_memcpy(str, str2, 5));

	str = strdup("Maman, est une belle femme");
	str2 = strdup("Marine");
	printf("#        - %s | %s               #\n", str, str2);
	printf("#            -> %-44s #\n", ft_memcpy(str, str2, 6));

	free(str);
	free(str2);

	/**
	* Test de ft_strdup
	**/
	printf("#    - ft_strdup:                                            #\n");
	str = ft_strdup("Pince mi et pince moi sont sur un bateau");
	printf("#        - %-49s #\n", str);
	str = ft_strdup("Pince mi tombe a l'eau...");
	printf("#        - %-49s #\n", str);
	str = ft_strdup("Qui est ce qui reste ?");
	printf("#        - %-49s #\n", str);


	printf("##############################################################\n");


	printf("\n");
	printf("\n");
	printf("\n");
	printf("##############################################################\n");
	printf("#                        PARTIE IV                           #\n");
	printf("#                          BONUS                             #\n");
	printf("##############################################################\n");
	/**
	* Test de ft_putchar
	**/
	printf("#    - ft_putchar:                                           #\n");
	printf("#        - ");
	ft_putchar('H');
	ft_putchar('e');
	ft_putchar('l');
	ft_putchar('l');
	ft_putchar('o');
	printf("                                             #\n");
	printf("#        - ");
	ft_putchar('T');
	ft_putchar('o');
	ft_putchar('u');
	ft_putchar('t');
	ft_putchar(' ');
	ft_putchar('l');
	ft_putchar('e');
	ft_putchar(' ');
	ft_putchar('m');
	ft_putchar('o');
	ft_putchar('n');
	ft_putchar('d');
	ft_putchar('e');
	printf("                                     #\n");

	/**
	* Test de ft_putstr
	**/
	printf("#    - ft_putstr:                                            #\n");
	printf("#       - ");
	ft_putstr("Pince mi et pince moi sont dans un bateau");
	printf("          #\n");
	printf("#       - ");
	ft_putstr("Pince mi tombe a l'eau");
	printf("                             #\n");
	printf("#       - ");
	ft_putstr("Qui est-ce qui reste ?");
	printf("                             #\n");


	/**
	* Test de ft_strnew
	**/
	printf("#    - ft_strnew:                                            #\n");
	char *tmp = ft_strnew(30);
	(void)tmp;
	char *res = malloc(31);
	bzero(res, 31);
	printf("#        - memcmp(ft_strnew(30), ft_bzero(31), 31) => %-6d #\n", memcmp(tmp, res, 31));
	free(tmp);
	free(res);
	tmp = ft_strnew(60);
	res = malloc(61);
	bzero(res, 61);
	printf("#        - memcmp(ft_strnew(60), ft_bzero(61), 61) => %-6d #\n", memcmp(tmp, res, 61));
	free(tmp);
	free(res);
	tmp = ft_strnew(260);
	res = malloc(261);
	bzero(res, 261);
	printf("#        - memcmp(ft_strnew(260), ft_bzero(261), 261) => %-3d #\n", memcmp(tmp, res, 261));
	free(tmp);
	free(res);

	/**
	* Test de ft_power
	**/
	printf("#    - ft_power:                                             #\n");
	printf("#        - 2 ^ 0 = %-41d #\n", ft_power(2, 0));
	printf("#        - 2 ^ 1 = %-41d #\n", ft_power(2, 1));
	printf("#        - 2 ^ 2 = %-41d #\n", ft_power(2, 2));
	printf("#        - 2 ^ 3 = %-41d #\n", ft_power(2, 3));
	printf("#        - 2 ^ 4 = %-41d #\n", ft_power(2, 4));
	printf("#        - 10 ^ 5 = %-40d #\n", ft_power(10, 5));

	/**
	* Test de ft_strchr
	**/
	printf("#    - ft_strchr:                                            #\n");
	printf("#       - Toto et tata -> e                                  #\n");
	printf("#            -> %-44s #\n", ft_strchr("Toto et tata", 'e'));
	printf("#       - sont dans un bateau -> u                           #\n");
	printf("#            -> %-44s #\n", ft_strchr("sont dans un bateau", 'u'));
	printf("#       - Lorem ipsum -> s                                   #\n");
	printf("#            -> %-44s #\n", ft_strchr("Lorem ipsum", 's'));

	printf("##############################################################\n");


	return (0);
}
