global _ft_cat


extern _ft_putstr
extern _ft_strnew
extern _ft_bzero
extern _read

; void		ft_cat(int fd);

_ft_cat:
	push rbp
	mov rbp, rsp
	sub rsp, 8

	push rdi
	mov rdi, 1023
	call _ft_strnew
	push rax
	xor rax, rax

	.while:
		pop rsi
		mov rdi, rsi
		push rsi
		mov rsi, 1024
		call _ft_bzero
		pop rsi
		pop rdi
		mov rdx, 1024
		push rsi
		call _read
		pop rsi
		push rdi
		push rsi
		mov rdi, rsi
		cmp rax, 0
		jle .end
		push rax
		call _ft_putstr
		pop rax
		add rdi, rax
		cmp rdi, 0
		je .end
		jmp .while

	.end:
		leave
		ret