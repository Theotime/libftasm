#ifndef LIBFT_H
# define LIBFT_H

# include <unistd.h>


extern void		ft_bzero(void *s, size_t n);
extern char		*ft_strcat(char *s1, const char *s2);
extern int		ft_isalpha(int c);
extern int		ft_isdigit(int c);
extern int		ft_isalnum(int c);
extern int		ft_isascii(int c);
extern int		ft_isprint(int c);
extern int		ft_tolower(int c);
extern int		ft_toupper(int c);
extern int		ft_puts(const char *s);

extern void		*ft_memset(void *b, int c, size_t len);
extern void		*ft_memcpy(void *s1, void const *s2, size_t n);
extern char		*ft_strdup(char const *s);
extern size_t	ft_strlen(char const *str);

extern void		ft_cat(int fd);


extern void		ft_putstr(const char *s);
extern void		ft_putchar(const char c);
extern char		*ft_strnew(size_t n);
extern int		ft_power(int x, int n);
extern char		*ft_strchr(char const *s, int c);

#endif
