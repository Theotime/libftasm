global _ft_tolower

; int		ft_tolower(int c);

_ft_tolower:
	mov rax, rdi
	cmp rdi, 65
	jl .end
	cmp rdi, 90
	jg .end
	add rax, 32
	.end:
		ret
