global _ft_power

; int		ft_power(int x, int n)

_ft_power:
	mov rax, 1
	cmp rsi, 0
	jle .end
	.while:
		dec rsi
		mul rdi
		cmp rsi, 0
		jne .while
	.end:
		ret