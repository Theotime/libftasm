global _ft_strnew

extern _malloc
extern _bzero

; char	*ft_strnew(size_t size)

_ft_strnew:
	push rbp
	mov rbp, rsp
	sub rsp, 8

	add rdi, 1
	push rdi
	call _malloc
	pop rsi
	mov rdi, rax
	push rax
	call _bzero
	pop rax
	leave
	ret