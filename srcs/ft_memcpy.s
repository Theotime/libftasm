global _ft_memcpy

; void		*ft_memcpy(void *s1, void const *s2, size_t n);

_ft_memcpy:
	mov rbx, rdi
	mov rcx, rdx
	rep movsb				; tant que rcx-- != 0
	mov rax, rbx
	ret
